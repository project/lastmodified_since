<?php

namespace Drupal\lastmodified_since;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the language manager service.
 */
class LastmodifiedSinceServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('http_middleware.page_cache');
    $definition->setClass('Drupal\lastmodified_since\StackMiddleware\LastModifiedSinceCache');
  }
}
<?php

namespace Drupal\lastmodified_since\StackMiddleware;

use Symfony\Component\HttpFoundation\Request;
use Drupal\page_cache\StackMiddleware\PageCache;

/**
 * Extending PageCache.
 */
class LastModifiedSinceCache extends PageCache {

  /**
   * {@inheritdoc}
   */
  protected function lookup(Request $request, $type = self::MASTER_REQUEST, $catch = TRUE) {

    if ($response = $this->get($request)) {
      $response->headers->set('X-Drupal-Cache', 'HIT');
    }
    else {
      $response = $this->fetch($request, $type, $catch);
    }

    // Only allow caching in the browser and prevent that the response is stored
    // by an external proxy server when the following conditions apply:
    // 1. There is a session cookie on the request.
    // 2. The Vary: Cookie header is on the response.
    // 3. The Cache-Control header does not contain the no-cache directive.
    if ($request->cookies->has(session_name()) &&
      in_array('Cookie', $response->getVary()) &&
      !$response->headers->hasCacheControlDirective('no-cache')
    ) {

      $response->setPrivate();
    }

    // Check content is modified and set 304 header.
    $response->isNotModified($request);

    return $response;
  }
}